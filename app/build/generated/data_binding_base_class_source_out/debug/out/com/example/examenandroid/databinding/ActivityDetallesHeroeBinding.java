// Generated by view binder compiler. Do not edit!
package com.example.examenandroid.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toolbar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.example.examenandroid.R;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityDetallesHeroeBinding implements ViewBinding {
  @NonNull
  private final CoordinatorLayout rootView;

  @NonNull
  public final FrameLayout FrameL2;

  @NonNull
  public final AppCompatImageView ImageHeroe;

  @NonNull
  public final NestedScrollView activityDetalle;

  @NonNull
  public final AppBarLayout appBar;

  @NonNull
  public final TabLayout tabs;

  @NonNull
  public final Toolbar toolbar;

  @NonNull
  public final CollapsingToolbarLayout toolbarLayout;

  @NonNull
  public final ViewPager2 vpContent;

  private ActivityDetallesHeroeBinding(@NonNull CoordinatorLayout rootView,
      @NonNull FrameLayout FrameL2, @NonNull AppCompatImageView ImageHeroe,
      @NonNull NestedScrollView activityDetalle, @NonNull AppBarLayout appBar,
      @NonNull TabLayout tabs, @NonNull Toolbar toolbar,
      @NonNull CollapsingToolbarLayout toolbarLayout, @NonNull ViewPager2 vpContent) {
    this.rootView = rootView;
    this.FrameL2 = FrameL2;
    this.ImageHeroe = ImageHeroe;
    this.activityDetalle = activityDetalle;
    this.appBar = appBar;
    this.tabs = tabs;
    this.toolbar = toolbar;
    this.toolbarLayout = toolbarLayout;
    this.vpContent = vpContent;
  }

  @Override
  @NonNull
  public CoordinatorLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityDetallesHeroeBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityDetallesHeroeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_detalles_heroe, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityDetallesHeroeBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.FrameL2;
      FrameLayout FrameL2 = rootView.findViewById(id);
      if (FrameL2 == null) {
        break missingId;
      }

      id = R.id.ImageHeroe;
      AppCompatImageView ImageHeroe = rootView.findViewById(id);
      if (ImageHeroe == null) {
        break missingId;
      }

      id = R.id.activity_Detalle;
      NestedScrollView activityDetalle = rootView.findViewById(id);
      if (activityDetalle == null) {
        break missingId;
      }

      id = R.id.app_bar;
      AppBarLayout appBar = rootView.findViewById(id);
      if (appBar == null) {
        break missingId;
      }

      id = R.id.tabs;
      TabLayout tabs = rootView.findViewById(id);
      if (tabs == null) {
        break missingId;
      }

      id = R.id.toolbar;
      Toolbar toolbar = rootView.findViewById(id);
      if (toolbar == null) {
        break missingId;
      }

      id = R.id.toolbar_layout;
      CollapsingToolbarLayout toolbarLayout = rootView.findViewById(id);
      if (toolbarLayout == null) {
        break missingId;
      }

      id = R.id.vpContent;
      ViewPager2 vpContent = rootView.findViewById(id);
      if (vpContent == null) {
        break missingId;
      }

      return new ActivityDetallesHeroeBinding((CoordinatorLayout) rootView, FrameL2, ImageHeroe,
          activityDetalle, appBar, tabs, toolbar, toolbarLayout, vpContent);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
