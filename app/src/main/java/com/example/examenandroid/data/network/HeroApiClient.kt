package com.example.examenandroid.data.network

import com.example.examenandroid.data.model.BiographyModel
import com.example.examenandroid.data.model.HeroModel
import com.example.examenandroid.data.model.StatsModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface HeroApiClient {
    @GET
    suspend fun getAllHero(@Url url:String):Response<HeroModel>

    @GET
    suspend fun getStats(@Url url:String):Response<StatsModel>

    @GET
    suspend fun getBiographys(@Url url:String):Response<BiographyModel>

}