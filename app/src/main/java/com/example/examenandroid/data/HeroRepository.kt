package com.example.examenandroid.data

import com.example.examenandroid.data.model.BiographyModel
import com.example.examenandroid.data.model.HeroModel
import com.example.examenandroid.data.model.HeroProvaider
import com.example.examenandroid.data.model.StatsModel
import com.example.examenandroid.data.network.HeroService

class HeroRepository {
    private val api = HeroService()

    suspend fun getAllHero(query: String): HeroModel {
        val response = api.getHero(query)
        HeroProvaider.heros = response!!
        return response
    }

    suspend fun getAllStats(query: String): StatsModel {
        val response = api.getStatsH(query)
        HeroProvaider.stats = response!!
        return response
    }

    suspend fun getAllBiography(query: String): BiographyModel {
        val response = api.getBiographyH(query)
        HeroProvaider.biography = response!!
        return response
    }
}