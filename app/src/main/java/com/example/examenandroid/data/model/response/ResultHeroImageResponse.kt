package com.example.examenandroid.data.model.response

import com.google.gson.annotations.SerializedName

data class ResultHeroImageResponse (
        @SerializedName("url") var url: String
)