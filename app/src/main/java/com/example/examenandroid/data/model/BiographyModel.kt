package com.example.examenandroid.data.model

import com.google.gson.annotations.SerializedName

data class BiographyModel (
    @SerializedName("response") val response: String,
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("full-name") val fullName: String,
    @SerializedName("alter-egos") val alterEgo: String,
    @SerializedName("aliases") val aliases: List<String>,
    @SerializedName("place-of-birth") val place: String,
    @SerializedName("first-appearance") val firsy: String,
    @SerializedName("publisher") val publisher: String,
    @SerializedName("alignment") val alignment: String

)