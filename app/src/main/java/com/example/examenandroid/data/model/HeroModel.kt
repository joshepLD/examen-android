package com.example.examenandroid.data.model

import com.example.examenandroid.data.model.response.ResultHeroResponse
import com.google.gson.annotations.SerializedName

data class HeroModel (
        @SerializedName("response") val response : String,
        @SerializedName("results-for") val result : String,
        @SerializedName("results") val results : ArrayList<ResultHeroResponse>
)