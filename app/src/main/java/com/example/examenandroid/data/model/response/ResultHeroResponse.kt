package com.example.examenandroid.data.model.response

import com.google.gson.annotations.SerializedName

data class ResultHeroResponse (
        @SerializedName("id") var id: String,
        @SerializedName("name") var name: String,
        @SerializedName("image") var imagen: ResultHeroImageResponse
)