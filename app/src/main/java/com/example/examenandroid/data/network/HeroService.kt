package com.example.examenandroid.data.network

import com.example.examenandroid.core.RetrofitHelper
import com.example.examenandroid.data.model.BiographyModel
import com.example.examenandroid.data.model.HeroModel
import com.example.examenandroid.data.model.StatsModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class HeroService {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getHero(query: String):HeroModel?{
        return withContext(Dispatchers.IO){
            val response = retrofit.create(HeroApiClient::class.java).getAllHero("search/$query")
            response.body()
        }
    }

    suspend fun getStatsH(query: String): StatsModel?{
        return withContext(Dispatchers.IO){
            val response = retrofit.create(HeroApiClient::class.java).getStats("$query/powerstats")
            response.body()
        }
    }

    suspend fun getBiographyH(query: String): BiographyModel?{
        return withContext(Dispatchers.IO){
            val response = retrofit.create(HeroApiClient::class.java).getBiographys("$query/biography")
            response.body()
        }
    }
}