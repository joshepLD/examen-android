package com.example.examenandroid.ui.view.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.examenandroid.databinding.FragmentStatsBinding
import com.example.examenandroid.ui.viewmodel.StatsViewModel

class StatsFragment(id: String) : Fragment() {
    private lateinit var binding: FragmentStatsBinding
    private var idHeroe: String = id
    private val statsViewModel: StatsViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        binding = FragmentStatsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        statsViewModel.statsModel.observe(this, Observer {
            binding.txtNombre.setText(it.name)
            binding.txtInteligencia.setText(it.intelligence)
            binding.txtFuerza.setText(it.strength)
            binding.txtVelocidad.setText(it.speed)
            binding.txtDurabilidad.setText(it.durability)
            binding.txtPoder.setText(it.power)
            binding.txtCombate.setText(it.combat)
        })
        statsViewModel.onCreate(idHeroe)
    }



}