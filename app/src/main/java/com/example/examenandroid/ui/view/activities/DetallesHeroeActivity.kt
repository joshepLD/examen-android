package com.example.examenandroid.ui.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels

import com.example.examenandroid.databinding.ActivityDetallesHeroeBinding
import com.example.examenandroid.databinding.ContentDetalleBinding
import com.example.examenandroid.ui.adapters.ViewPagerAdapter
import com.example.examenandroid.ui.viewmodel.DetallesHeroeViewModel
import com.google.android.material.tabs.TabLayoutMediator
import com.squareup.picasso.Picasso

class DetallesHeroeActivity : AppCompatActivity() {
    private lateinit var binding:ActivityDetallesHeroeBinding
    private lateinit var adapter:ViewPagerAdapter
    private val detallesHeroeViewModel: DetallesHeroeViewModel by viewModels()

    private var idHero: String? = null
    private var url: String? = null
    private var name: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetallesHeroeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        idHero = intent.getStringExtra("idHeroe")
        url = intent.getStringExtra("urlImagen")
        name = intent.getStringExtra("name")

        Picasso.get().load(url).into(binding.ImageHeroe)
        binding.toolbar.setTitle(name)
        adapter = ViewPagerAdapter(supportFragmentManager, lifecycle, idHero.toString())
        binding.vpContent.adapter = adapter
        detallesHeroeViewModel.createTabs(binding.tabs, binding.vpContent)
    }
}