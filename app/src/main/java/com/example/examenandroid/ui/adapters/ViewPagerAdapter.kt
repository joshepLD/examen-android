package com.example.examenandroid.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.examenandroid.ui.view.fragments.AparienciaFragment
import com.example.examenandroid.ui.view.fragments.BiografiaFragment
import com.example.examenandroid.ui.view.fragments.StatsFragment
import com.example.examenandroid.ui.view.fragments.WorksFragment

class ViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle, id:String): FragmentStateAdapter(fragmentManager, lifecycle) {
    var idHeroe:String = id

    override fun getItemCount(): Int {
        return 4
    }

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> {
                StatsFragment(idHeroe)
            }
            1 -> {
                BiografiaFragment(idHeroe)
            }
            2 -> {
                AparienciaFragment()
            }
            3 -> {
                WorksFragment()
            }
            else -> {
                Fragment()
            }
        }
    }
}