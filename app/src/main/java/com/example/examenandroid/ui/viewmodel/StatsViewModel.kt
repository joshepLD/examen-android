package com.example.examenandroid.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.examenandroid.data.model.StatsModel
import com.example.examenandroid.domain.GetStatsUse
import kotlinx.coroutines.launch

class StatsViewModel(): ViewModel() {
    var getStatsUse = GetStatsUse()
    val statsModel = MutableLiveData<StatsModel>()

    fun onCreate(query: String) {
        viewModelScope.launch {
            val result = getStatsUse(query)
            statsModel.postValue(result)
        }
    }
}