package com.example.examenandroid.ui.view.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.examenandroid.databinding.FragmentBiografiaBinding
import com.example.examenandroid.ui.viewmodel.BiografiaViewModel

class BiografiaFragment(id: String) : Fragment() {
    private lateinit var binding: FragmentBiografiaBinding
    private val biografiaViewModel: BiografiaViewModel by viewModels()
    private var idHeroe: String = id

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentBiografiaBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        biografiaViewModel.biographyModel.observe(this, Observer {
            binding.txtNombre.setText(it.name)
            binding.txtNombreC.setText(it.fullName)
            binding.txtAlterEgo.setText(it.alterEgo)
            binding.txtLugarN.setText(it.place)
            binding.txtPrimeraA.setText(it.firsy)
            binding.txtEditor.setText(it.publisher)
            binding.txtAliniacion.setText(it.alignment)
        })
        biografiaViewModel.onCreate(idHeroe)
    }
}