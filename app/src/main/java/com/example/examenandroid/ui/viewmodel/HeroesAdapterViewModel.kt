package com.example.examenandroid.ui.viewmodel

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.examenandroid.data.model.response.ResultHeroResponse
import com.example.examenandroid.databinding.ItemHeroeBinding
import com.example.examenandroid.ui.view.activities.DetallesHeroeActivity
import com.squareup.picasso.Picasso

class HeroesAdapterViewModel(view: View):RecyclerView.ViewHolder(view) {
    private val binding = ItemHeroeBinding.bind(view)

    fun bind(data: ResultHeroResponse){
        val name:String = data.name
        val urlImagen:String = data.imagen.url
        val id:String = data.id

        Picasso.get().load(urlImagen).into(binding.ivIHero)
        binding.txtNombre.setText(name)

        binding.itemHero.setOnClickListener {
            val intent = Intent(it.context, DetallesHeroeActivity::class.java)
            intent.putExtra("idHeroe", id)
            intent.putExtra("urlImagen", urlImagen)
            intent.putExtra("name", name)
            it.context.startActivity(intent)
        }
    }
}