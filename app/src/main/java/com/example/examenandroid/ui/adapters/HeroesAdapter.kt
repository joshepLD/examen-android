package com.example.examenandroid.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.examenandroid.R
import com.example.examenandroid.data.model.response.ResultHeroResponse
import com.example.examenandroid.ui.viewmodel.HeroesAdapterViewModel

class HeroesAdapter(val heroes: ArrayList<ResultHeroResponse>): RecyclerView.Adapter<HeroesAdapterViewModel>() {
    private lateinit var listener: View.OnClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroesAdapterViewModel {
        val layoutInflater = LayoutInflater.from(parent.context)
        return HeroesAdapterViewModel(layoutInflater.inflate(R.layout.item_heroe, parent, false))
    }
    override fun getItemCount(): Int = heroes.size

    override fun onBindViewHolder(holder: HeroesAdapterViewModel, position: Int) {
        val item = heroes[position]
        holder.bind(item)
    }

}