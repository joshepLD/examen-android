package com.example.examenandroid.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.examenandroid.data.model.HeroModel
import com.example.examenandroid.domain.GetHeroUse
import kotlinx.coroutines.launch

class HeroViewModel(): ViewModel() {
    var getHeroUse = GetHeroUse()
    val heroModel = MutableLiveData<HeroModel>()
    val isLoading  = MutableLiveData<Boolean>()
    val isMessage  = MutableLiveData<Boolean>()

    fun onCreate(query: String) {
        viewModelScope.launch {
            isMessage.postValue(false)
            isLoading.postValue(true)
            val result = getHeroUse(query)
            heroModel.postValue(result)
            isLoading.postValue(false)

        }
    }

}