package com.example.examenandroid.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.examenandroid.data.model.BiographyModel
import com.example.examenandroid.domain.GetBiographyUse
import com.example.examenandroid.domain.GetStatsUse
import kotlinx.coroutines.launch

class BiografiaViewModel(): ViewModel() {
    var getBiographyUse = GetBiographyUse()
    val biographyModel = MutableLiveData<BiographyModel>()

    fun onCreate(query: String) {
        viewModelScope.launch {
            val result = getBiographyUse(query)
            biographyModel.postValue(result)
        }
    }
}