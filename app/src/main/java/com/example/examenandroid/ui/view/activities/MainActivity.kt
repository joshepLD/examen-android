package com.example.examenandroid.ui.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager

import androidx.activity.viewModels
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.examenandroid.data.model.response.ResultHeroResponse
import com.example.examenandroid.databinding.ActivityMainBinding
import com.example.examenandroid.ui.adapters.HeroesAdapter
import com.example.examenandroid.ui.viewmodel.HeroViewModel

class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: HeroesAdapter
    private var heroes = ArrayList<ResultHeroResponse>()
    private val heroViewModel: HeroViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.searchHero.setOnQueryTextListener(this)
        setContentView(binding.root)
        initRecyclerView()

        heroViewModel.heroModel.observe(this, Observer {
            Log.e("--->", it.results.toString())
            heroes.clear()
            heroes.addAll(it.results)
            adapter.notifyDataSetChanged()
        })

        heroViewModel.isLoading.observe(this, Observer {
            binding.progress.isVisible = it
        })

        heroViewModel.isMessage.observe(this, Observer {
            binding.messageInicial.isVisible = it
        })
    }

    private fun initRecyclerView(){
        adapter = HeroesAdapter(heroes)
        binding.rvHeroes.layoutManager = LinearLayoutManager(this)
        binding.rvHeroes.adapter = adapter
    }

    private fun hideKeyboard(){
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.viewRoot.windowToken, 0)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if(!query.isNullOrEmpty()){
            heroViewModel.onCreate(query.toLowerCase())
            hideKeyboard()
        }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }
}