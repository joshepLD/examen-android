package com.example.examenandroid.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class DetallesHeroeViewModel(): ViewModel() {

    fun createTabs(tabs: TabLayout, vpContent: ViewPager2) {
        TabLayoutMediator(tabs, vpContent ){tab, position ->
            when(position){
                0 ->{
                    tab.text="Estadísticas"
                }
                1 ->{
                    tab.text="Biografía"
                }
                2 ->{
                    tab.text="Apariencia"
                }
                3 ->{
                    tab.text="Trabajos"
                }
            }
        }.attach()
    }
}