package com.example.examenandroid.core

import android.provider.Settings.System.getString
import com.example.examenandroid.R
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitHelper {
    fun getRetrofit():Retrofit{
        return Retrofit.Builder()
            .baseUrl("https://www.superheroapi.com/api.php/4559115720838954/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}