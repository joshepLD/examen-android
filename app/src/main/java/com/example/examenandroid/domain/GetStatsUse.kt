package com.example.examenandroid.domain

import com.example.examenandroid.data.HeroRepository
import com.example.examenandroid.data.model.StatsModel

class GetStatsUse(){
    private val repository = HeroRepository()

    suspend operator fun invoke(query: String):StatsModel = repository.getAllStats(query)

}