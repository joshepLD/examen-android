package com.example.examenandroid.domain

import com.example.examenandroid.data.HeroRepository
import com.example.examenandroid.data.model.BiographyModel

class GetBiographyUse(){
    private val repository = HeroRepository()

    suspend operator fun invoke(query: String): BiographyModel = repository.getAllBiography(query)

}