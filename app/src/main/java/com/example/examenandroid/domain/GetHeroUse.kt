package com.example.examenandroid.domain

import com.example.examenandroid.data.HeroRepository
import com.example.examenandroid.data.model.HeroModel

class GetHeroUse(){
    private val repository = HeroRepository()

    suspend operator fun invoke(query: String):HeroModel = repository.getAllHero(query)

}